/**
 * Base classes and interfaces that domain entities may extend, handling common
 * issues such as identity, auditing and read-only reference-type entities, e.g. states.
 *
 */
package com.expressui.core.entity;